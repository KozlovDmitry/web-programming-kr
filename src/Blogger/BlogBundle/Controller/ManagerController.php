<?php


namespace Blogger\BlogBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ManagerController extends Controller
{
    public function indexAction(){
        return $this->render('BloggerBlogBundle:Manager:index.html.twig');
    }
}